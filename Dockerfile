FROM gradle:alpine as build
ADD build.gradle .
ADD resources resources
ADD src src
RUN gradle build

FROM openjdk:8-jre-alpine
COPY --from=build /home/gradle/build/libs/my-application.jar /root/my-application.jar
WORKDIR /root
CMD ["java", "-server", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-XX:InitialRAMFraction=2", "-XX:MinRAMFraction=2", "-XX:MaxRAMFraction=2", "-XX:+UseG1GC", "-XX:MaxGCPauseMillis=100", "-XX:+UseStringDeduplication", "-jar", "my-application.jar"]
